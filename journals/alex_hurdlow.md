## 06-26-23

Day 1 of coding. Working out the repo on gitlab and starting on auth. Trying to fix the pipeline error currently.

## 06-27-23

Finished up user stories, mostly, and then started on Auth.

## 06-28-23

Finished Auth backend. Project planning on how to proceed.

## 06-29-23

Implemented get_all and create for posts.py in routers and queries.

## 06-30-23

Implemented get_user_posts in our posts.py for routers and queries.

## 07-10-23

Implemented get_one, update, and deleted routers and queries for our posts.

## 07-11-23

Finished routes and queries for profiles. Backend complete! Need to go back and do more specific error handling, but otherwise everything is functioning as intended. Helped contribute on the profile routes and queries by realizing we would need two different get queries, one for the logged in user and one where you can select an individual profile that is not your own. The update query then calls the get_self function to get the profile id needed for the db query.

## 07-12-23

Had to go back to fronend to add the account_username property to our PostOut pydantic mode. Assisted with debugging the broken code from that change. Finally, as a group we set the framework for how we want to divide and conquer the frontend, including creating a skeleton of all the .js files we'll need.

## 07-13-23

Rewatched redux lectures

## 07-14-23

Continued to rewatch reacut/redux lectures

## 07-17-23

Finished up the homepage for front end. Split up into groups, David, Kolby, and I will be working on login and front end auth, and Nick and Tifa will work on cleaning up the homepage and creating some of the create forms.

## 07-18-23

Working on signup, attempting to create a multistep wizard form that exists on the same route.

## 07-19-23

Unfortunately, the above failed, and we were recommended by instructors to scrap what we had and switch over to redux to better manage state in our app.

## 07-20-23

Signup completed! Fixed various bugs regarding mypage, editing a post, and deleting a post.

## 07-24-23

Finished the edit profile page. Also implemented the functionality for all buttons across are various pages, and cleaned up code and optimized multiple pages.

## 07-25-23

Started and finished our unit tests, also started on the README.md

## 07-26-23

Lots and lots of debugging, error handling, and styling across the entire app. I primarily implemented some backend error handling with profiles, and then assisted some frontend debugging and optimization on a few pages.

## 07-27-23

Went in and added a get all profiles route and query in the backend. This allowed me to be able to implement a feature where the profile picture and pet name can be added to every post/postcard instance on our frontend. Required refactoring a lot of our .map function but it appears to be working fine!

## 07-28-23

Final day of debugging. Helped fix an issue with our redirect for a user clicking on their own name from the homepage so that it takes them to their mypage instead of the generic otheruser view. Passed pipeline tests for lint-test-job and api-unit-test-job. Project complete!! Just need to deploy.
