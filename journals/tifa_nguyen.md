## Monday, June 26, 2023

- Cloned project repo
- Fixed pipeline issue (account verification)
- Tried merging to main branch to verify everything works
- Worked on userstories

## Tuesday, June 27, 2023

- Chose our database (postgresql) and got docker to work
- Started implementing auth (create new account)
  - Got stuck at the 401 unauthorized (able to see input data via print statements, but unsure why the error is raised)

## Wednesday, June 28, 2023

- Resolved the 401 issue (just need to implement the 'get' function inside 'AccountQueries')
- Finished backend for auth (create account, login, logout, getting tokens for logged in user, protected route example)
- Got the api endpoint placeholders for 'posts' to work (get all posts, create, update, delete)

## Thursday, June 29, 2023

- Got the post (create a post) endpoint to work
- Got the get (get all posts) endpoint to work

## Friday, June 30, 2023

- Got the get (get posts for specific user) endpoint to work

## Monday, July 10, 2023

- Got the update a post endpoint to work
- Same as delete a post

## Tuesday, July 11, 2023

- Completed API enpoints for create, update, and get user profile

## Wednesday, July 12, 2023

- Refactored PostOut to now include username so we can easily display on frontend
- Updated all functions and queries (with INNER JOIN) accordingly
- Had a simple frontend page to display all posts to test if our data works properly
- Rewatched Redux lectures
- Created placeholders for all of our pages

## Thursday, July 13, 2023

- Continued rewatching RTK lectures

## Friday, July 14, 2023

- Continued rewatching RTK lectures

## Monday, July 17, 2023

- Set up Redux store and slice for auth as well as home page (to get all posts)

## Tuesday, July 18, 2023

- Created frontend for 'create post'
- Created protected route component to redirect non-logged in userr to 'login' page
- Worked with Nick to create frontend for specific user's page
- Working on updating a post. Getting 422 error

## Wednesday, July 19, 2023

- Resolved 422 error (note: builder.mutation query only takes in one parameter)
- Workig on multi-step signup form (unable to render info from previous page)

## Thursday, July 20, 2023

- Got signup, login, logout to work on frontend (decided to simplify the multi-step sign up page)
- Added back and front ends for 'mypage'

## Friday, July 21, 2023

- No class

## Monday, July 24, 2023

- Took a personal day

## Tuesday, July 25, 2023

- Created unit tests
- Working on README

## Wednesday, July 26, 2023

- Working on error handlings, redirects, and adding more little features to front end
- Revamp api routes and updating docs

## Thursday, July 27, 2023

- Working on styling
- Exploring the 'likes' feature as stretch goal (used React front end at first but data was not being saved so need to add backend for it )

## Friday, July 28, 2023

- Code cleanup
- Added minor changes (UI, redirect fixes)
- Integrated unit tests in pipeline
