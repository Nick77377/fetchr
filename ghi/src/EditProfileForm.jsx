import { useEffect, useState } from "react";
import onChange from "react";
import {
  useGetLoggedInProfileQuery,
  useUpdateProfileMutation,
} from "./app/apiSlice";
import { useNavigate } from "react-router-dom";
import Breeds from "./Signup/Breeds";

function UpdateProfileForm() {
  const navigate = useNavigate();
  const [update] = useUpdateProfileMutation();
  const { data, isLoading } = useGetLoggedInProfileQuery();
  const [profile_picture, setProfilePicture] = useState("");
  const [pet_name, setPetName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [gender, setGender] = useState("");
  const [breed, setBreed] = useState("");
  const [bio, setBio] = useState("");

  useEffect(() => {
    if (data) {
      setProfilePicture(data.profile_picture);
      setPetName(data.pet_name);
      setBirthday(data.birthday);
      setGender(data.gender);
      setBreed(data.breed);
      setBio(data.bio);
    }
  }, [data]);

  if (isLoading) {
    return <div>Is Loading...</div>;
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const body = {
      profile_picture: profile_picture,
      pet_name: pet_name,
      birthday: birthday,
      gender: gender,
      breed: breed,
      bio: bio,
    };

    update(body);

    setTimeout(() => {
      navigate("/mypage");
    }, 1000);
  };

  const cancelButton = () => {
    navigate(`/mypage`);
  };

  return (
    <div className="container-fluid">
    <div className='bg-custom'></div>
    <div className='background-accent shadow-lg'></div>
    <div className='background-accent-2 shadow-lg'></div>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Update Profile</h1>
          <form onSubmit={handleSubmit} id="update-post-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setProfilePicture(e.target.value)}
                value={profile_picture}
                placeholder="profile picture URL"
                required
                type="text"
                name="profile_picture"
                id="profile_picture"
                className="form-control"
              />
              <label htmlFor="profile_picture">Profile Picture</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setPetName(e.target.value)}
                value={pet_name}
                placeholder="Pet Name"
                required
                type="text"
                name="pet_name"
                id="pet_name"
                className="form-control"
              />
              <label htmlFor="pet_name">Pet Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setBirthday(e.target.value)}
                value={birthday}
                placeholder="Birthday"
                required
                type="date"
                name="birthday"
                id="birthday"
                className="form-control"
              />
              <label htmlFor="birthday">Birthday</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setGender(e.target.value)}
                value={gender}
                placeholder="Gender"
                required
                type="text"
                name="gender"
                id="gender"
                className="form-control"
              />
              <label htmlFor="gender">Gender</label>
            </div>
            <div className="form-floating mb-3">
              <select
                id="breed"
                className="form-select"
                required
                aria-label={Breeds.label}
                onChange={(e) => {
                  setBreed(e.target.value);
                  onChange(e.target.value, Breeds.value);
                }}
                value={breed}
                name="breed"
              >
                <option value="">{Breeds.label}</option>
                {Breeds.map((optionValue, index) => (
                  <option key={index} value={optionValue}>
                    {optionValue}
                  </option>
                ))}
              </select>
              <label htmlFor="options">Breed</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setBio(e.target.value)}
                value={bio}
                placeholder="Bio"
                required
                type="text"
                name="bio"
                id="bio"
                className="form-control"
              />
              <label htmlFor="gender">Bio</label>
            </div>
            <button type="submit" className="btn-transition-sm gradient">
              Update
            </button>
            <button className="btn-transition-sm gradient" onClick={cancelButton}>
              Cancel
            </button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
}

export default UpdateProfileForm;
