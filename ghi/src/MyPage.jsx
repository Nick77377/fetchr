import { useState, useEffect } from "react";
import { useGetMyPostsQuery, useGetLoggedInProfileQuery } from "./app/apiSlice";
import PostCard from "./PostCard";
import ProfileCard from "./ProfileCard";
import { useNavigate } from "react-router-dom";

function MyPage() {
  const navigate = useNavigate();
  const { data: postsData, isLoading: postLoading } = useGetMyPostsQuery();
  const { data: profileData, isLoading: profileLoading } =
    useGetLoggedInProfileQuery();

  const [profileLookup, setProfileLookup] = useState({});

  const editButton = () => {
    navigate("/profile/edit");
  };
  const createProfileButton = () => {
    navigate("/profile");
  };
  const editPostButton = (post_id) => {
    navigate(`/posts/${post_id}/update`);
  };
  const createPostButton = () => {
    navigate("/posts/create");
  };

  useEffect(() => {
    if (!profileLoading && profileData) {
      const lookup = {};
      if (typeof profileData === "object") {
        lookup[profileData.account_id] = profileData;
        setProfileLookup(lookup);
      }
    }
  }, [profileLoading, profileData]);

  if (postLoading || profileLoading) {
    return <div>Loading...</div>;
  }

  const hasProfile = !("ERROR!" in profileData);

  return (
    <>
      <div>
        {hasProfile ? (
          <container>
            <ProfileCard
              profile_picture={profileData.profile_picture}
              pet_name={profileData.pet_name}
              bio={profileData.bio}
              breed={profileData.breed}
              birthday={profileData.birthday}
              gender={profileData.gender}
            />
            <div
              className="row justify-content-center"
              style={{ marginRight: "0", marginLeft: "0" }}
            >
              <div className="col-3 d-flex p-4 justify-content-center">
                <button
                  className="btn-transition-sm gradient shadow m-2"
                  style={{ width: "10rem" }}
                  onClick={editButton}
                >
                  Edit profile
                </button>
                <button
                  className="btn-transition-sm gradient shadow m-2"
                  style={{ width: "10rem" }}
                  onClick={createPostButton}
                >
                  Create a post!
                </button>
              </div>
            </div>
          </container>
        ) : (
          <div style={{ fontSize: "1.4rem" }}>
            User hasn't created a profile yet 😭
            <button className="btn btn-primary" onClick={createProfileButton}>
              Create Profile
            </button>
          </div>
        )}
      </div>
      <div className="my-5 d-flex flex-column align-items-center">
        <div className="bg-custom"></div>
        <div className="background-accent shadow-lg"></div>
        <div className="background-accent-2 shadow-lg"></div>
        {postsData && Array.isArray(postsData) && postsData.length > 0 ? (
          postsData.map((post) => (
            <div key={post.id} className="mt-4">
              {profileLookup[post.account_id] ? (
                <PostCard
                  profilePicture_url={
                    profileLookup[post.account_id].profile_picture
                  }
                  pet_name={profileLookup[post.account_id].pet_name}
                  picture_url={post.picture_url}
                  description={post.description}
                  post_date={post.post_date}
                  account_id={post.account_id}
                  editPostButton={() => editPostButton(post.id)}
                  showEditButton={true}
                />
              ) : null}
            </div>
          ))
        ) : (
          <div style={{ fontSize: "1.4rem" }}>
            You don't have any posts yet 😭
          </div>
        )}
      </div>
    </>
  );
}

export default MyPage;
