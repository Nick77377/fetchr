import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const fetchrApi = createApi({
  reducerPath: "fetchrApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getAccount: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      transformResponse: (response) => (response ? response.account : null),
      providesTags: ["Account"],
    }),

    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: "/token",
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account"],
    }),

    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),

    signup: builder.mutation({
      query: (body) => ({
        url: "api/accounts",
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),

    getAllPosts: builder.query({
      query: () => "/posts",
      providesTags: ["Posts"],
    }),

    createPost: builder.mutation({
      query: (body) => ({
        url: "/posts",
        body,
        method: "POST",
        credentials: "include",
      }),
      invalidatesTags: ["Posts"],
    }),

    createProfile: builder.mutation({
      query: (body) => ({
        url: "/profile",
        body,
        method: "POST",
        credentials: "include",
      }),
      invalidatesTags: ["Profiles"],
    }),

    updateProfile: builder.mutation({
      query: (body) => ({
        url: `/profile`,
        body,
        method: "PUT",
        credentials: "include",
      }),
      invalidatesTags: ["Profiles"],
    }),

    getUserPosts: builder.query({
      query: (account_id) => ({
        url: `/accounts/${account_id}/posts`,
        credentials: "include",
      }),
      providesTags: ["Posts"],
    }),

    getMyPosts: builder.query({
      query: () => ({
        url: "/myposts",
        credentials: "include",
      }),
      providesTags: ["Posts"],
    }),

    getSpecificUserProfile: builder.query({
      query: (account_id) => ({
        url: `/profiles/${account_id}/`,
        credentials: "include",
      }),
    }),

    getLoggedInProfile: builder.query({
      query: () => ({
        url: `/profile`,
        credentials: "include",
      }),
      providesTags: ["Profiles"],
    }),

    getAllProfiles: builder.query({
      query: () => "/profiles",
      providesTags: ["Profiles"],
    }),

    updatePost: builder.mutation({
      query: ({ post_id, body }) => ({
        url: `/posts/${post_id}`,
        body,
        method: "PUT",
        credentials: "include",
      }),
      invalidatesTags: ["Posts"],
    }),

    getOnePost: builder.query({
      query: (post_id) => ({
        url: `/posts/${post_id}`,
        credentials: "include",
      }),
      providesTags: ["Posts"],
    }),

    deletePost: builder.mutation({
      query: (post_id) => ({
        url: `/posts/${post_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Posts"],
    }),
  }),
});

export const {
  useGetAccountQuery,
  useLoginMutation,
  useSignupMutation,
  useLogoutMutation,
  useGetAllPostsQuery,
  useCreatePostMutation,
  useGetUserPostsQuery,
  useGetSpecificUserProfileQuery,
  useUpdatePostMutation,
  useGetOnePostQuery,
  useDeletePostMutation,
  useGetMyPostsQuery,
  useCreateProfileMutation,
  useUpdateProfileMutation,
  useGetLoggedInProfileQuery,
  useGetAllProfilesQuery,
} = fetchrApi;
