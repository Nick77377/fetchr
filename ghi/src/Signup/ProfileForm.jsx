import { useState } from "react";
import { useCreateProfileMutation } from "../app/apiSlice";
import { useNavigate } from "react-router-dom";
import Breeds from "./Breeds";
import onChange from "react";

const ProfileForm = () => {
  const [create_profile] = useCreateProfileMutation();
  const [profile_picture, setProfilePicture] = useState("");
  const [pet_name, setPetName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [gender, setGender] = useState("");
  const [breed, setBreed] = useState("");
  const [bio, setBio] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    create_profile({ profile_picture, pet_name, birthday, gender, breed, bio });
    navigate("/mypage");
  };

  return (
    <div className="container">
    <div className="row">
      <div className='background-accent shadow-lg'></div>
      <div className='background-accent-2 shadow-lg'></div>
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Signup</h1>
          <form onSubmit={handleSubmit} id="signup-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setProfilePicture(e.target.value)}
                value={profile_picture}
                placeholder="profile picture URL"
                required
                type="text"
                name="profile_picture"
                id="profile_picture"
                className="form-control"
                maxLength={500}
              />
              <label htmlFor="profile_picture">Profile Picture</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setPetName(e.target.value)}
                value={pet_name}
                placeholder="Pet Name"
                required
                type="text"
                name="pet_name"
                id="pet_name"
                className="form-control"
                maxLength={50}
              />
              <label htmlFor="pet_name">Pet Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setBirthday(e.target.value)}
                value={birthday}
                placeholder="Birthday"
                required
                type="date"
                name="birthday"
                id="birthday"
                className="form-control"
              />
              <label htmlFor="birthday">Birthday</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setGender(e.target.value)}
                value={gender}
                placeholder="Gender"
                required
                type="text"
                name="gender"
                id="gender"
                className="form-control"
                maxLength={10}
              />
              <label htmlFor="gender">Gender</label>
            </div>
            <div className="form-floating mb-3">
              <select
                id="breed"
                className="form-select"
                required
                aria-label={Breeds.label}
                onChange={(e) => {
                  setBreed(e.target.value);
                  onChange(e.target.value, Breeds.value);
                }}
                value={breed}
                name="breed"
              >
                <option value="">{Breeds.label}</option>
                {Breeds.map((optionValue, index) => (
                  <option key={index} value={optionValue}>
                    {optionValue}
                  </option>
                ))}
              </select>
              <label htmlFor="options">Breed</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setBio(e.target.value)}
                value={bio}
                placeholder="Bio"
                required
                type="text"
                name="bio"
                id="bio"
                className="form-control"
                maxLength={500}
              />
              <label htmlFor="bio">Bio</label>
            </div>
            <button className="btn-transition-sm gradient" type="Submit">
              Create Profile
            </button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
};

export default ProfileForm;
