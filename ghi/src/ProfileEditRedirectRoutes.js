import React from "react";
import { Navigate, useLocation } from "react-router-dom";
import { useGetAccountQuery } from "./app/apiSlice";
import { useGetLoggedInProfileQuery } from "./app/apiSlice";

const ProfileEditRedirectRoutes = ({ child }) => {
  const { data: account, isLoading } = useGetAccountQuery();
  const { data: profile, isLoading: profileLoading } =
    useGetLoggedInProfileQuery();
  const location = useLocation();

  if (isLoading || profileLoading) return <div>Loading...</div>;

  if (!account) {
    return <Navigate to="/login/" replace />;
  } else if (
    account &&
    location.pathname === `/profile` &&
    !("ERROR!" in profile)
  ) {
    return <Navigate to="/profile/edit/" replace />;
  } else {
    return child;
  }
};

export default ProfileEditRedirectRoutes;
