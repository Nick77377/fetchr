import { useNavigate } from "react-router-dom";
import { useLogoutMutation, useGetAccountQuery } from "./app/apiSlice";
import fetchrlogo from "./fetchrlogo.svg"

const Nav = () => {
  const navigate = useNavigate();
  const [logout] = useLogoutMutation();
  const { data: account } = useGetAccountQuery();

  const onLogout = () => {
    logout();
    navigate("/login");
  };

  const homepageButton = () => {
    navigate("/");
  };

  const loginButton = () => {
    navigate("/login");
  };

  const CreateAccountButton = () => {
    navigate("/signup");
  };

  const myPageButton = () => {
    navigate("/mypage");
  };

  const CreatePostButton = () => {
    navigate("/posts/create")
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark mb-4 shadow sticky-top bg-white">
      <button className="m-2 logo d-flex" onClick={homepageButton}>
        <p className="m-1 logo-text align-self-start align-self-center ">fetchr</p>
        <img alt='fetchrlogo' className="align-self-start align-self-center" src={fetchrlogo} style={{height: '7vh', width: '7vh'}}></img>
      </button>
      {account ? (
        <>
          <button className="btn-transition gradient mx-2" onClick={CreatePostButton}>
            📝 Create post
          </button>
          <button className="btn-transition gradient mx-2" onClick={myPageButton}>
           My page
          </button>
          <div className="" style={{width: '65vw'}}>
          </div>
          <div className="">
            <button className="btn-transition gradient mx-2" onClick={onLogout}>
              Logout
            </button>
          </div>
        </>
      ) : (
        <>
          <div className="" style={{width: '65vw'}}>
          </div>
          <div>
            <button className="btn-transition gradient mx-2" onClick={loginButton}>
              Login
            </button>
            <button className="btn-transition gradient mx-2" onClick={CreateAccountButton}>
              Create an account
            </button>
          </div>
        </>
      )}
    </nav>
  );
};

export default Nav;
