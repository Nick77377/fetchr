import React, { useState } from "react";
// Stretch goal
export default function LikeButton() {
  const [like, setLike] = useState(0);
  const [unLike, setUnLike] = useState(0);
  const [liked, setLiked] = useState(false);

  return (
    <div>
      {!liked && (
        <h5
          onSubmit={() => {
            setLike(like + 1);
            setLiked(true);
          }}
        >
          ♥️
        </h5>
      )}
      {liked && (
        <h5
          onSubmit={() => {
            setLike(like - 1);
            setLiked(false);
          }}
        >
          💖
        </h5>
      )}
      <h6>{like} likes</h6>
    </div>
  );

  // return (
  //   <div>
  //     <h5 onClick={() => setLike((prevState) => !prevState)}>
  //       Like: {like ? "💖" : "♥️"}
  //     </h5>
  //   </div>
  // );
}

// export default function LikeButton() {
//   const [likes, setLikes] = useState(0);
//   const [liked, setLiked] = useState(false);
//   return (
//     <button
//       className={`like-button ${liked ? "liked" : ""}`}
//       onClick={() => {
//         setLikes(likes + 1);
//         setLiked(true);
//       }}
//     >
//       {likes} Likes
//     </button>
//   );
// }
