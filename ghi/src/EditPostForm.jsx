import { useEffect, useState } from "react";
import {
  useGetAccountQuery,
  useUpdatePostMutation,
  useGetOnePostQuery,
  useDeletePostMutation,
} from "./app/apiSlice";
import { useNavigate, useParams } from "react-router-dom";

function EditPostForm() {
  const navigate = useNavigate();
  const [update] = useUpdatePostMutation();
  const params = useParams();
  const { data, isLoading } = useGetOnePostQuery(params.post_id);
  const [description, setDescription] = useState("");
  const { data: account } = useGetAccountQuery();
  const [Delete] = useDeletePostMutation();

  useEffect(() => {
    if (data) {
      setDescription(data.description);
    }
  }, [data]);

  if (isLoading) {
    return <div>Is Loading...</div>;
  }
  if (account.id !== data.account_id)
    return <div> You do not have access to modify this post. </div>;

  const handleSubmit = (e) => {
    e.preventDefault();
    const body = {
      description: description,
    };
    const post_id = params.post_id;

    update({
      post_id,
      body,
    });

    navigate("/mypage");
  };

  const handleDelete = (e) => {
    const post_id = params.post_id;
    Delete(post_id);
  };

  const cancelButton = () => {
    navigate(`/mypage`);
  };

  return (
    <div className="container-fluid">
      <div className="bg-custom"></div>
      <div className="background-accent shadow-lg"></div>
      <div className="background-accent-2 shadow-lg"></div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Update Post</h1>
            <form onSubmit={handleSubmit} id="update-post-form">
              <div className="form-floating mb-3">
                <textarea
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                  placeholder="Description"
                  required
                  type="text"
                  name="description"
                  id="description"
                  className="form-control"
                />
                <label htmlFor="description">Description</label>
              </div>
              <button type="submit" className="btn-transition-sm gradient">
                Update
              </button>
              <button
                className="btn-transition-sm gradient"
                onClick={handleDelete}
              >
                Delete
              </button>
              <button
                className="btn-transition-sm gradient"
                onClick={cancelButton}
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditPostForm;
