import { useState, useEffect } from "react";
import { useGetAllPostsQuery, useGetAllProfilesQuery } from "./app/apiSlice";
import PostCard from "./PostCard";

const HomePage = () => {
  const { data: profileStuff, isLoading: profileLoading } =
    useGetAllProfilesQuery();
  const { data: postStuff, isLoading: postLoading } = useGetAllPostsQuery();

  const [profileLookup, setProfileLookup] = useState({});

  useEffect(() => {
    if (!profileLoading && profileStuff) {
      const lookup = {};
      profileStuff.forEach((profile) => {
        lookup[profile.account_id] = profile;
      });
      setProfileLookup(lookup);
    }
  }, [profileLoading, profileStuff]);

  if (postLoading || profileLoading) return <div>Loading...</div>;

  return (
    <>
      <div className="my-5 d-flex flex-column align-items-center">
        <div className="bg-custom"></div>
        <div className="background-accent shadow-lg"></div>
        <div className="background-accent-2 shadow-lg"></div>
        {postStuff.map((post) => (
          <div key={post.id} className="mt-4">
            {profileLookup[post.account_id] ? (
              <PostCard
                profilePicture_url={
                  profileLookup[post.account_id].profile_picture
                }
                pet_name={profileLookup[post.account_id].pet_name}
                picture_url={post.picture_url}
                description={post.description}
                post_date={post.post_date}
                account_id={post.account_id}
              />
            ) : null}
          </div>
        ))}
      </div>
    </>
  );
};

export default HomePage;
