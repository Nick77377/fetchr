import { Link } from "react-router-dom";
import { useState } from "react";

const PostCard = ({
  picture_url,
  pet_name,
  description,
  post_date,
  account_id,
  editPostButton,
  showEditButton,
  profilePicture_url,
}) => {
  const [showMore, setShowMore] = useState(false);
  const showMoreButton = description.length >= 100;
  return (
    <>
      <div className="col-3">
        <div
          className="card mb-3 shadow-lg"
          style={{ width: "30rem", height: "40rem" }}
        >
          <div className="card-header" style={{ height: "8vh" }}>
            <div className="row justify-content-end">
              <div className="position-absolute d-flex">
                <img
                  alt="profile pic"
                  src={profilePicture_url}
                  className="rounded-circle shadow-sm align-self-start p-1"
                  style={{ height: "6vh", width: "6vh" }}
                ></img>
                <h5 className="align-self-end p-2" style={{ zIndex: "2" }}>
                  <Link
                    className="link-unstyled"
                    to={`/accounts/${account_id}/posts`}
                  >
                    {pet_name}
                  </Link>
                </h5>
              </div>
              <div className="" style={{ zIndex: "1" }}>
                <div>
                  {showEditButton && (
                    <div className="text-end">
                      <button
                        className="btn"
                        type="button"
                        id="dropdownMenuButton2"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        <i className="bi bi-list"></i>
                      </button>
                      <ul
                        className="dropdown-menu"
                        aria-labelledby="dropdownMenuButton2"
                      >
                        <button
                          className="dropdown-item dropdown-btn btn text-black"
                          onClick={editPostButton}
                        >
                          Edit Post
                        </button>
                      </ul>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <img
            alt=""
            src={picture_url}
            className="card-img-top"
            style={{ height: "20rem" }}
          />
          <div className="card-body">
            <div className="row">
              <h5 className="card-title fs-6 mb-3">
                <Link
                  className="link-unstyled"
                  to={`/accounts/${account_id}/posts`}
                >
                  {pet_name}
                </Link>
              </h5>
            </div>
            <p className="card-text border-top">
              {showMore ? description : `${description.substring(0, 40)}`}
              {showMoreButton && (
                <button
                  className="text-muted text-sm-left btn-readmore"
                  onClick={() => setShowMore(!showMore)}
                >
                  {showMore ? "less" : "more"}
                </button>
              )}
            </p>
            <div className="text-muted">{post_date}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PostCard;
