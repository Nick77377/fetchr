### Log in

- Endpoint path: /token
- Endpoint method: POST

- Request shape (form):

  - username: string
  - password: string

- Response: Account information and a token
- Response shape (JSON):
  ```json
  {
    "account": {
      "username": string,
      "password": string
    },
    "token": string
  }
  ```

### Log out

- Endpoint path: /token
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Always true
- Response shape (JSON):
  ```json
  true
  ```

### Get a list of posts (from all users)

- Endpoint path: /posts
- Endpoint method: GET

- Response: A list of posts
- Response shape:

  ```json
  [
    {
      "description": string,
      "picture_url": string,
      "id": int,
      "account_id": int,
      "account_username": string,
      "post_date": date
    }
  ]

  ```

### Get a list of posts (from a specific user)

- Endpoint path: /accounts/{account_id}/posts
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of posts (from that specific user)
- Response shape:

  ```json
  [
    {
      "description": string,
      "picture_url": string,
      "id": int,
      "account_id": int,
      "account_username": string,
      "post_date": date
    }
  ]

  ```

  ### Get a list of posts for currently logged in user

- Endpoint path: /myposts
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of posts (from that specific user)
- Response shape:
  ```json
  [
    {
      "description": string,
      "picture_url": string,
      "id": int,
      "account_id": int,
      "account_username": string,
      "post_date": date
    }
  ]
  ```

### Get one post

- Endpoint path: /posts/{post_id}
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of posts (from that specific user)
- Response shape:
  ```json
  [
    {
      "description": string,
      "picture_url": string,
      "id": int,
      "account_id": int,
      "account_username": string,
      "post_date": date
    }
  ]
  ```

### Create a new post

- Endpoint path: /posts
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "picture_url": string,
    "description": string
  }
  ```

- Response: An indication of success or failure
- Response shape:
  ```json
  {
    "description": string,
    "picture_url": string,
    "id": int,
    "account_id": int,
    "account_username": string,
    "post_date": date
  }
  ```

### Update a post

- Endpoint path: /posts/{post_id}
- Endpoint method: PUT

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "description": string
  }

  ```

- Response: An indication of success or failure
- Response shape:
  ```json
  {
    "description": string,
    "picture_url": string,
    "id": int,
    "account_id": int,
    "account_username": string,
    "post_date": date
  }
  ```

### Delete a post

- Endpoint path: /posts/{post_id}
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: An indication of success or failure
- Response shape:
  ```json
  {
    boolean
  }
  ```

### Create profile

- Endpoint path: /profile
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string
  }

  ```

- Response: A newly created profile
- Response shape:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string,
    "id": int,
    "account_id": int
  }

  ```

## Update profile

- Endpoint path: /profile
- Endpoint method: PUT

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string
  }

  ```

- Response: An updated profile
- Response shape:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string,
    "id": int,
    "account_id": int
  }

  ```

## Get currently logged in user's profile

- Endpoint path: /profile
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: The currently logged in user's profile
- Response shape:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string,
    "id": int,
    "account_id": int
  }

  ```

## Get any user's profile

- Endpoint path: /profiles/{account_id}
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: Get any user's profile
- Response shape:

  ```json
  {
    "profile_picture": string,
    "pet_name": string,
    "birthday": date,
    "gender": string,
    "breed": string,
    "bio": string,
    "id": int,
    "account_id": int
  }

  ```

### Get a list of profiles (from all users)

- Endpoint path: /profiles
- Endpoint method: GET

- Response: A list of profiles
- Response shape:

  ```json
  [
    {
      "profile_picture": string,
      "pet_name": string,
      "birthday": date,
      "gender": string,
      "breed": string,
      "bio": string,
      "id": int,
      "account_id": int
    }
  ]

  ```
