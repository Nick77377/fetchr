from pydantic import BaseModel


class LikeIn(BaseModel):
    likes: int


class LikeOut(LikeIn):
    id: int
    post_id: int
    account_id: int
    user_who_liked_id: int
