from pydantic import BaseModel
from datetime import date


class PostIn(BaseModel):
    description: str
    picture_url: str


class PostOut(PostIn):
    id: int
    account_id: int
    account_username: str
    post_date: date


class PostInUpdate(BaseModel):
    description: str
