steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE posts (
            id SERIAL PRIMARY KEY NOT NULL,
            post_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            picture_url VARCHAR(500) NOT NULL,
            description VARCHAR(300) NOT NULL,
            account_id SERIAL NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE posts;
        """,
    ],
]
