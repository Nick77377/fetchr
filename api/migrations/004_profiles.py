steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE profiles (
            id SERIAL PRIMARY KEY NOT NULL,
            profile_picture VARCHAR(500) NOT NULL,
            pet_name VARCHAR(50) NOT NULL,
            birthday DATE,
            gender VARCHAR(10) NOT NULL,
            breed VARCHAR(150) NOT NULL,
            bio VARCHAR(500) NOT NULL,
            account_id SERIAL NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE profiles;
        """,
    ],
]
