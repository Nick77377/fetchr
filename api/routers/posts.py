from fastapi import (
    Depends,
    APIRouter,
)

from authenticator import authenticator
from queries.posts import PostQueries
from models.posts import PostIn, PostOut, PostInUpdate
from typing import List, Union
from models.error_handler import Error

router = APIRouter()


@router.get("/posts", response_model=Union[List[PostOut], Error])
def get_posts_all(
    queries: PostQueries = Depends(),
):
    return queries.get_all()


@router.get("/accounts/{account_id}/posts")
def get_user_posts(
    account_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: PostQueries = Depends(),
):
    return queries.get_user_posts(account_id)


@router.get("/myposts")
def get_my_posts(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: PostQueries = Depends(),
):
    return queries.get_user_posts(account_data["id"])


@router.get("/posts/{post_id}")
def get_one_post(
    post_id: int,
    queries: PostQueries = Depends(),
):
    return queries.get_one(post_id)


@router.post("/posts")
def create_post(
    post: PostIn,
    queries: PostQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.create(post, account_data)


@router.put("/posts/{post_id}")
def update_post(
    post_id: int,
    post: PostInUpdate,
    queries: PostQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.update(post_id, post, account_data)


@router.delete("/posts/{post_id}")
def delete_post(
    post_id: int,
    queries: PostQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.delete(post_id, account_data)
